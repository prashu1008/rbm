<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;



class RoleController extends Controller
{

    public function show()
    {
        //DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
       // $users = DB::select('select * from users');
       $roles = Role::all();
        return view('tag', ['roles' => $roles]);
    }
    public function create()
    {
        $roles = new Role;
       return view('edit_tag');

    }
    public function store(Request $request){

        $roles =new Role;
    Role::create($request->all());
    return redirect('/');
    }
}
