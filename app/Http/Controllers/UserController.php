<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Response;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function show()
    {
        //DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
       // $users = DB::select('select * from users');
       $users = User::all();
        return view('user', ['users' => $users]);
    }

    public function create()
    {
        //$user= new User();
        //$user->username= $request['username'];

    // add other fields


       // $users=User::create(['name'=>'john']);

       return view('add_user');
       //user::create($request->all());
    }

    public function store(Request $request){


    User::create($request->all());
    return redirect('/');
    }
}
