<?php

use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/userl', 'UserController@show');

Route::get('/user/create','UserController@create' );

Route::post('/user', 'UserController@store');

Route::get('/edit/create', 'RoleController@create' );

Route::post('/edit','RoleController@store');

Route::get('/tagl','RoleController@show');
