<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Relations</title>


</head>
<body><table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Relationships</th>
    </tr>

    </thead>
    <tbody>
    @foreach($roles as $role)
        <tr>

            <td>{{$role->name1}}</td>
            <td>{{$role->role}}</td>
            <td>{{$role->name2}}</td>

        </tr>
    @endforeach
    </tbody>
</table>

</body>
</html>

