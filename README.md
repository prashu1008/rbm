For Getting Degree of relations between two people,
I would go for a approach of using dynamic programming using memoization.

Tree based approach.

Client enters Name1 and Name2 .
Tree of relations be made from Name1 and Name2.

while traversing Left tree of Name1 we will store the roles of people in an array (memoization),which 
will help us in discarding those relations from right traversal of name2 ,which are already traversed in left subtree.

ADVANTAGES-
1)Not all users are needed to be traversed- Fast processing in case of huge data.
2)No same relation people are traversed more than once- Makes it even faster.
